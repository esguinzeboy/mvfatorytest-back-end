﻿using Microsoft.EntityFrameworkCore;
using MVFactoryEvaluation.DataAccesObject.Abstract;
using MVFactoryEvaluation.DataAccessLayer;
using MVFactoryEvaluation.DataAccessLayer.Abstract;
using MVFactoryEvaluation.Enitys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MVFactoryEvaluation.DataAccesObject
{
    public class AlertRepository : MasterRepository<Alert, Guid>, IAlertRepository
    { 
        public AlertRepository(IManagerContextInstanciator ManagerContext) : base(ManagerContext)
        {
        }

        public List<Alert> GetWithAllChildresn()
        {


            var alerts= this.DbSet.Include(x=> x.Header_Texts)
                      .Include(x=> x.Description_Texts)
                      .Include(x=> x.Informed_Entities)
                      .Include(x=> x.Active_Periods)
                      .ToList();


            return alerts;
        }

        public bool AlertExist(Alert alert)
        {

        
            //TODO crear comparativa compleja ya que no trae id unico para comparar


            //var exist = this.DbSet.Include(x => x.Header_Texts)
            //          .Include(x => x.Description_Texts)
            //          .Include(x => x.Informed_Entities)
            //          .Include(x => x.Active_Periods)
            //          .Where(x=> x.Active_Periods.Count ==alert.Active_Periods.Count
            //                 && x.Cause_Id== alert.Cause_Id 
            //                 && x.Effect== alert.Effect
            //                 && x.Active_Periods.Intersect(alert.Active_Periods).Any()
            //                 && x.Description_Texts.Intersect(alert.Description_Texts).Any()
            //                 && x.Informed_Entities.Intersect(alert.Informed_Entities).Any()
            //                 )
            //          .ToList();
            //

            return false;
        }

    }
}
