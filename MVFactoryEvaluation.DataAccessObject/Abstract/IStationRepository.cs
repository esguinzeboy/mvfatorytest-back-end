﻿using MVFactoryEvaluation.DataAccessLayer.Abstract;
using MVFactoryEvaluation.Enitys;
using System;
using System.Collections.Generic;
using System.Text;

namespace MVFactoryEvaluation.DataAccesObject.Abstract
{
    public interface IStationRepository : IMasterRepository<Station,int>
    {
    }
}
