﻿using MVFactoryEvaluation.DataAccessLayer.Abstract;
using MVFactoryEvaluation.Enitys;
using System;
using System.Collections.Generic;
using System.Text;

namespace MVFactoryEvaluation.DataAccesObject.Abstract
{
    public interface ICauseRepository : IMasterRepository<Cause,int>
    {
    }
}
