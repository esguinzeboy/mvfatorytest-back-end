﻿using MVFactoryEvaluation.DataAccesObject.Abstract;
using MVFactoryEvaluation.DataAccessLayer;
using MVFactoryEvaluation.DataAccessLayer.Abstract;
using MVFactoryEvaluation.Enitys;
using System;
using System.Collections.Generic;
using System.Text;

namespace MVFactoryEvaluation.DataAccesObject
{
    public class CauseRepository : MasterRepository<Cause, int>, ICauseRepository
    {
        public CauseRepository(IManagerContextInstanciator ManagerContext) : base(ManagerContext)
        {
        }
    }
}
