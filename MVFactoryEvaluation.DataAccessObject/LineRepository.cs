﻿using Microsoft.EntityFrameworkCore;
using MVFactoryEvaluation.DataAccesObject.Abstract;
using MVFactoryEvaluation.DataAccessLayer;
using MVFactoryEvaluation.DataAccessLayer.Abstract;
using MVFactoryEvaluation.Enitys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MVFactoryEvaluation.DataAccesObject
{
    public class LineRepository : MasterRepository<Line, int>, ILineRepository
    {
        public LineRepository(IManagerContextInstanciator ManagerContext) : base(ManagerContext)
        {
        }

        public List<Line> GetAllWithChilds()
        {
            var alerts = this.DbSet.Include(x => x.Stations)
                  .ToList();


            return alerts;
        }
    }
}
