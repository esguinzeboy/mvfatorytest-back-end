﻿using MVFactoryEvaluation.DataAccesObject.Abstract;
using MVFactoryEvaluation.DataAccessLayer;
using MVFactoryEvaluation.DataAccessLayer.Abstract;
using MVFactoryEvaluation.Enitys;
using System;
using System.Collections.Generic;
using System.Text;

namespace MVFactoryEvaluation.DataAccesObject
{
    public class EffectRepository : MasterRepository<Effect, int>, IEffectRepository
    {
        public EffectRepository(IManagerContextInstanciator ManagerContext) : base(ManagerContext)
        {
        }
    }
}
