﻿using MVFactoryEvaluation.Common.DTOS.JsonTimezonConveter;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVFactoryEvaluation.Common.DTOS
{
    public class ArrivalDepartureDTO
    {
        [JsonProperty("time")]
        [JsonConverter(typeof(TimeAdjustmentEpochConverter))]
        public DateTime Time { get; set; }

        [JsonProperty("delay")]
        public int Delay { get; set; }
    }
}
