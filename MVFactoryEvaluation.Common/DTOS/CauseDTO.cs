﻿namespace MVFactoryEvaluation.Common.DTOS
{
    public class CauseDTO 
    {
        public int Cause_id { get; set; }
        public string Description { get; set; }
    }
}