﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVFactoryEvaluation.Common.DTOS
{
    public class LineDTO
    {
        [JsonProperty("Trip_Id")]
        public string Trip_Id { get; set; }
        [JsonProperty("Route_Id")]
        public string Route_Id { get; set; }
        [JsonProperty("Direction_ID")]
        public int Direction_Id { get; set; }

        public string Direction_Description { get; set; }

        [JsonProperty("start_time")]
        public string Start_time { get; set; }
        [JsonProperty("start_date")]
        public string Start_date { get; set; }
        [JsonProperty("Estaciones")]
        public List<StationDTO> Stations { get; set; }
    }
}
