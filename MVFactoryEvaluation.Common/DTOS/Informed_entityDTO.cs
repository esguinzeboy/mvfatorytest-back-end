﻿using Newtonsoft.Json;
using System;

namespace MVFactoryEvaluation.Common.DTOS
{
    public class Informed_entityDTO
    {
        [JsonProperty("agency_id")]
        public string Agency_Id { get; set; }
        [JsonProperty("route_id")]
        public string Rout_Id { get; set; }
        [JsonProperty("route_type")]
        public int Route_Type { get; set; }
        [JsonProperty("stop_id")]
        public string Stop_Id { get; set; }
    }
}