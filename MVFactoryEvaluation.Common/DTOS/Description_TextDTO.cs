﻿using Newtonsoft.Json;
using System;

namespace MVFactoryEvaluation.Common.DTOS
{
    public class Description_TextDTO
    {
        [JsonProperty("text")]
        public string Text { get; set; }
        [JsonProperty("language")]
        public string Language { get; set; }
   
      //  public bool isHeader { get; set; }
    }
}