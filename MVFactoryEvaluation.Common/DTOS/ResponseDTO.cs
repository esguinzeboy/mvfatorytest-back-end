﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MVFactoryEvaluation.Common.DTOS
{
    public class ResponseDTO<T> where T :class
    {
        public T DtoToReturn { get; set; }
        public int StatusCode { get; set; }
        public string Messege { get; set; }

    }
}
