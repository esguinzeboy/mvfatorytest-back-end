﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVFactoryEvaluation.Common.DTOS
{
    public class StationDTO
    {
        [JsonProperty("stop_id")]
        public string Stop_id { get; set; }
        [JsonProperty("stop_name")]
        public string Stop_name { get; set; }
        [JsonProperty("arrival")]
        public ArrivalDepartureDTO Arrival { get; set; }
        [JsonProperty("departure")]
        public ArrivalDepartureDTO Departure { get; set; }
        
        public int Line_Id { get; set; }
        public LineDTO Line { get; set; }


    }
}
