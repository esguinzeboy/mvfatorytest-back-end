﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Text;

namespace MVFactoryEvaluation.Common.DTOS.JsonTimezonConveter
{
    public class TimeAdjustmentEpochConverter : DateTimeConverterBase
    {
       
        // Fix GMT issue 
        // TODO resolve issue from startup
       
        private static readonly DateTime _epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Local);


        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var date = (DateTime)value;
            //var dateIntoString = date.ToString("yyyy-MM-ddTHH\\:mm\\:ss.fffffffzzz");
            var dateIntoString = date.ToString("yyyyMMddHHmmss");
             writer.WriteRawValue(dateIntoString);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            
            if (reader.Value == null) { return null; }
            var utcOffset = TimeZoneInfo.Local.GetUtcOffset(DateTime.UtcNow);
            var totalOfSeconds = (long)reader.Value + (long)utcOffset.TotalSeconds;
            return _epoch.AddSeconds(totalOfSeconds);
        }
    }
}
