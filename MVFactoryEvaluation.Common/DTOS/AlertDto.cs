﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;


namespace MVFactoryEvaluation.Common.DTOS
{
    public class AlertDTO
    {
        [JsonProperty("active_period")]
        public List<Active_PeriodDTO> Active_Periods { get; set; }
        [JsonProperty("informed_entity")]
        public List<Informed_entityDTO> Informed_entity { get; set; }
        [JsonProperty("cause")]
        public int Cause { get; set; }
        [JsonProperty("effect")]
        public int Effect { get; set; }
        [JsonProperty("header_text")]
        public List<Header_TextDTO> Header_Texts { get; set; }
        [JsonProperty("description_text")]
        public List<Description_TextDTO> Description_Texts { get; set; }

    }
}
