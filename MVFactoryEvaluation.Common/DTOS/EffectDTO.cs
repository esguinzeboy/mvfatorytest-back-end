﻿namespace MVFactoryEvaluation.Common.DTOS
{
    public class EffectDTO
    {
        public int Effect_id { get; set; }
        public string Description { get; set; }
    }
}