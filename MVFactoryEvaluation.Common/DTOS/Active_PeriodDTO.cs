﻿using MVFactoryEvaluation.Common.DTOS.JsonTimezonConveter;
using Newtonsoft.Json;
using System;

namespace MVFactoryEvaluation.Common.DTOS
{
    public class Active_PeriodDTO
    {

        [JsonProperty("start")]
        [JsonConverter(typeof(TimeAdjustmentEpochConverter))]
        public DateTime Start { get; set; }
        [JsonProperty("end")]
        [JsonConverter(typeof(TimeAdjustmentEpochConverter))]
        public DateTime End { get; set; }
    }
}