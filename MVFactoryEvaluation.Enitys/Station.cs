﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MVFactoryEvaluation.Enitys
{
    public class Station : BaseEntity<int>
    {
        public virtual string Stop_id { get; set; }
        public virtual string Stop_name { get; set; }

        public virtual int Line_Id { get; set; }
        public virtual Line Line { get; set; }
    }
}
