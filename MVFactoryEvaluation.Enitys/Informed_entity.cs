﻿using System;
using System.Collections.Generic;

namespace MVFactoryEvaluation.Enitys
{
    public class Informed_entity: BaseEntity<Guid>
    {
 
        public string Agency_Id { get; set; }
        public string Rout_Id { get; set; }
        public int Route_Type { get; set; }
        public string Stop_Id { get; set; }

        //public virtual List<Alert> Alerts { get; set; }

        public  Guid Alert_Id { get; set; }
        public  Alert Alert { get; set; }

    }
}