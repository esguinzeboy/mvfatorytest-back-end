﻿using System.Collections.Generic;

namespace MVFactoryEvaluation.Enitys
{
    public class Cause : BaseEntity<int>
    {
        public virtual int Cause_id { get; set; }
        public virtual string Description { get; set; }
        public virtual List<Alert> Alerts { get; set; }
    }
}