﻿using System;

namespace MVFactoryEvaluation.Enitys
{
    public class Description_Text: BaseEntity<Guid>
    {

        public virtual string Text { get; set; }
        public virtual string Language { get; set; }
        //public virtual bool isHeader { get; set; }
        public virtual Guid Alert_Id { get; set; }
        public virtual Alert Alert { get; set; }
    }
}