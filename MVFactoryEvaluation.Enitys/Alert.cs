﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MVFactoryEvaluation.Enitys
{
   public class Alert : BaseEntity<Guid>
    {
        
        public  List<Active_Period> Active_Periods { get; set; }
        
        //public virtual Guid informed_entity_id { get; set; }
               
        //public virtual Informed_entity informed_entity { get; set; }

       public  List<Informed_entity> Informed_Entities { get; set; }

        public  int Cause_Id { get; set; }

        public  Cause Cause { get; set; }

        public  int Effect_Id { get; set; }

        public  Effect Effect { get; set; }
               
        public  List<Header_Text> Header_Texts { get; set; }
               
        public  List<Description_Text> Description_Texts { get; set; }

    }
}
