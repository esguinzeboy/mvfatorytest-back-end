﻿using System;

namespace MVFactoryEvaluation.Enitys
{
    public class Active_Period : BaseEntity<Guid>
    {

        public virtual DateTime Start { get; set; }
        public virtual DateTime End { get; set; }
        public virtual Guid Alert_Id { get; set;} 
        public virtual Alert Alert { get; set; }
    }
}