﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MVFactoryEvaluation.Enitys
{
    public class BaseEntity<T>
    {
        public virtual T Id { get; set; }
        public virtual DateTime CreationDate { get; set; }
        public virtual DateTime? UpdateTime { get; set; }
        public virtual DateTime? CessetionDate { get; set; }
    }
}
