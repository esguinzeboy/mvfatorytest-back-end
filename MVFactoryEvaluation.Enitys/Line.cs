﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace MVFactoryEvaluation.Enitys
{

    public class Line: BaseEntity<int>
    {
       // public virtual string Trip_Id { get; set; }
        public virtual string Route_Id { get; set; }
        public virtual int Direction_Id { get; set; }
        public virtual string Direction_Description { get; set; }
        public virtual List<Station> Stations { get; set; }

    }
}
