﻿using System.Collections.Generic;

namespace MVFactoryEvaluation.Enitys
{
    public class Effect: BaseEntity<int>
    {
        public int Effect_id { get; set; }
        public string Description { get; set; }
        public virtual List<Alert> Alerts { get; set; }
    }
}