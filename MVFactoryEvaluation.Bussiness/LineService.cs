﻿using AutoMapper;
using Microsoft.Extensions.Configuration;
using MVFactoryEvaluation.Bussiness.Abstract;
using MVFactoryEvaluation.Common.DTOS;
using MVFactoryEvaluation.DataAccesObject.Abstract;
using MVFactoryEvaluation.Enitys;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Web;

namespace MVFactoryEvaluation.Bussiness
{
    public class LineService : ILineService
    {
        IConfiguration Configuration;
        ILineRepository LineRepository;
        IMapper mapper;

        public LineService(IConfiguration _configuration, ILineRepository lineRepository, IMapper Mapper)
        {
            Configuration = _configuration;
            LineRepository = lineRepository;
            mapper = Mapper;
        }

        public ResponseDTO<List<LineDTO>> GetAll()
        {
            ResponseDTO<List<LineDTO>> responseDto = new ResponseDTO<List<LineDTO>>();
            List<LineDTO> lineDTOs = new List<LineDTO>();
            var listOfLines = LineRepository.GetAll();
            foreach (var item in listOfLines)
            {
                lineDTOs.Add(new LineDTO
                {
                     Direction_Id= item.Direction_Id,
                      Route_Id= item.Route_Id
                });
            }
            responseDto.DtoToReturn = lineDTOs;
            responseDto.StatusCode = 200;
            responseDto.Messege = "resultado OK";
            return responseDto;

        }

        public List<LineDTO> GetAllFull() => mapper.Map<List<Line>, List<LineDTO>>(LineRepository.GetAllWithChilds());
        

        public ResponseDTO<LineDTO> GetLineById()
        {
            throw new NotImplementedException();
        }


    }
}
