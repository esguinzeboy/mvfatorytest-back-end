﻿using Microsoft.Extensions.Configuration;
using MVFactoryEvaluation.Bussiness.Abstract;
using MVFactoryEvaluation.Common.DTOS;
using MVFactoryEvaluation.DataAccesObject.Abstract;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Web;

namespace MVFactoryEvaluation.Bussiness
{
    public class CauseService : ICauseService
    { 
       
        IConfiguration Configuration;
        ICauseRepository CauseRepository;

        public CauseService(IConfiguration _configuration, ICauseRepository _CauseRepository)
        {
            Configuration = _configuration;
             CauseRepository = _CauseRepository;
           
        }

        public ResponseDTO<List<CauseDTO>> GetAll()
        {
            ResponseDTO<List<CauseDTO>> responseDto = new ResponseDTO<List<CauseDTO>>();
            List<CauseDTO> CauseDtos = new List<CauseDTO>();
            var listOfCause = CauseRepository.GetAll();
            foreach (var item in listOfCause)
            {
                CauseDtos.Add(new CauseDTO
                {
                      Cause_id= item.Cause_id,
                       Description= item.Description
                });
            }
            responseDto.DtoToReturn = CauseDtos;
            responseDto.StatusCode = 200;
            responseDto.Messege = "resultado OK";
            return responseDto;

        }

        public ResponseDTO<CauseDTO> GetLineById()
        {
            throw new NotImplementedException();
        }


    }
}
