﻿using Microsoft.Extensions.Configuration;
using MVFactoryEvaluation.Bussiness.Abstract;
using MVFactoryEvaluation.Common.DTOS;
using MVFactoryEvaluation.DataAccesObject.Abstract;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Web;

namespace MVFactoryEvaluation.Bussiness
{
    public class EffectService : IEffectService
    { 
       
        IConfiguration Configuration;
        IEffectRepository EffectRepository;

        public EffectService(IConfiguration _configuration, IEffectRepository _EffectRepository)
        {
            Configuration = _configuration;
             EffectRepository = _EffectRepository;
           
        }

        public ResponseDTO<List<EffectDTO>> GetAll()
        {
            ResponseDTO<List<EffectDTO>> responseDto = new ResponseDTO<List<EffectDTO>>();
            List<EffectDTO> EffectDtos = new List<EffectDTO>();
            var listOfEffect = EffectRepository.GetAll();
            foreach (var item in listOfEffect)
            {
                EffectDtos.Add(new EffectDTO
                {
                      Effect_id= item.Effect_id,
                       Description= item.Description
                });
            }
            responseDto.DtoToReturn = EffectDtos;
            responseDto.StatusCode = 200;
            responseDto.Messege = "resultado OK";
            return responseDto;

        }

        public ResponseDTO<EffectDTO> GetLineById()
        {
            throw new NotImplementedException();
        }


    }
}
