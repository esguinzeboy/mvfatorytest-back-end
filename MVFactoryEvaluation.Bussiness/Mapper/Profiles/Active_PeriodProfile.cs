﻿using AutoMapper;
using MVFactoryEvaluation.Common.DTOS;
using MVFactoryEvaluation.Enitys;
using System;
using System.Collections.Generic;
using System.Text;

namespace MVFactoryEvaluation.Bussiness.Mapper.Profiles
{
    class Active_PeriodProfile : Profile
    {
        public Active_PeriodProfile()
        {
            CreateMap<Active_PeriodDTO, Active_Period>() 
                  .ForMember(x => x.Start, opt => opt.MapFrom(m=> m.Start))
                  .ForMember(x => x.End, opt => opt.MapFrom(m=> m.End))
                  .ForMember(x => x.Alert, opt => opt.Ignore())
                 .ForMember(x => x.Alert_Id, opt => opt.Ignore())
                 .ForMember(x => x.CessetionDate, opt => opt.Ignore())
                 .ForMember(x => x.UpdateTime, opt => opt.Ignore())
                 .ForMember(x => x.CreationDate, opt => opt.Ignore())
                 .ForMember(x => x.Id, opt => opt.Ignore()).ReverseMap();



        }
    }
}
