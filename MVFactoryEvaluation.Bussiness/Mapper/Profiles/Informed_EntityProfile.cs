﻿using AutoMapper;
using MVFactoryEvaluation.Common.DTOS;
using MVFactoryEvaluation.Enitys;
using System;
using System.Collections.Generic;
using System.Text;

namespace MVFactoryEvaluation.Bussiness.Mapper.Profiles
{
    class Informed_EntityProfile : Profile
    {
        public Informed_EntityProfile()
        {
                       
            CreateMap<Informed_entityDTO, Informed_entity>()
                  .ForMember(x => x.Agency_Id, opt => opt.MapFrom(m=> m.Agency_Id))
                  .ForMember(x => x.Route_Type, opt => opt.MapFrom(m=> m.Route_Type))
                  .ForMember(x => x.Rout_Id, opt => opt.MapFrom(m=> m.Rout_Id))
                  .ForMember(x => x.Stop_Id, opt => opt.MapFrom(m=> m.Stop_Id))
                  .ForMember(x => x.Alert, opt => opt.Ignore())
                 .ForMember(x => x.Alert_Id, opt => opt.Ignore())
                 .ForMember(x => x.CessetionDate, opt => opt.Ignore())
                 .ForMember(x => x.UpdateTime, opt => opt.Ignore())
                 .ForMember(x => x.CreationDate, opt => opt.Ignore())
                 .ForMember(x => x.Id, opt => opt.Ignore()).ReverseMap();
                 



        }
    }
}
