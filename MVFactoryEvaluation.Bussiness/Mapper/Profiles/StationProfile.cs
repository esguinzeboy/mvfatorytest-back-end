﻿using AutoMapper;
using MVFactoryEvaluation.Common.DTOS;
using MVFactoryEvaluation.Enitys;
using System;
using System.Collections.Generic;
using System.Text;

namespace MVFactoryEvaluation.Bussiness.Mapper.Profiles
{
    class StationProfile : Profile
    {
        public StationProfile()
        {
            CreateMap<StationDTO, Station>() 
                  .ForMember(x => x.Stop_id, opt => opt.MapFrom(m=> m.Stop_id))
                  .ForMember(x => x.Stop_name, opt => opt.MapFrom(m=> m.Stop_name))
                  .ForMember(x => x.Line, opt => opt.MapFrom(m=> m.Line))
                  .ForMember(x => x.Line_Id, opt => opt.MapFrom(m=> m.Line_Id))
                 .ForMember(x => x.CessetionDate, opt => opt.Ignore())
                 .ForMember(x => x.UpdateTime, opt => opt.Ignore())
                 .ForMember(x => x.CreationDate, opt => opt.Ignore())
                 .ForMember(x => x.Id, opt => opt.Ignore()).ReverseMap();


            CreateMap<StationDTO, Station>().ReverseMap()
                 .ForMember(x => x.Arrival, opt => opt.Ignore())
                 .ForMember(x => x.Departure, opt => opt.Ignore());



        }
    }
}
