﻿using AutoMapper;
using MVFactoryEvaluation.Common.DTOS;
using MVFactoryEvaluation.Enitys;
using System;
using System.Collections.Generic;
using System.Text;

namespace MVFactoryEvaluation.Bussiness.Mapper.Profiles
{
    class DescriptionTextProfile : Profile
    {
        public DescriptionTextProfile()
        {

            CreateMap<Description_TextDTO, Description_Text>()
                  .ForMember(x => x.Text, opt => opt.MapFrom(m=> m.Text))
                  .ForMember(x => x.Language, opt => opt.MapFrom(m=> m.Language))
                 // .ForMember(x => x.isHeader, opt => opt.MapFrom(m=> m.isHeader))
                  .ForMember(x => x.Alert, opt => opt.Ignore())
                 .ForMember(x => x.Alert_Id, opt => opt.Ignore())
                 .ForMember(x => x.CessetionDate, opt => opt.Ignore())
                 .ForMember(x => x.UpdateTime, opt => opt.Ignore())
                 .ForMember(x => x.CreationDate, opt => opt.Ignore())
                 .ForMember(x => x.Id, opt => opt.Ignore()).ReverseMap();

            //CreateMap<Description_Text, Description_TextDTO>()
            //    .ForMember(x => x.isHeader, opt => opt.Ignore());

        }
    }
}
