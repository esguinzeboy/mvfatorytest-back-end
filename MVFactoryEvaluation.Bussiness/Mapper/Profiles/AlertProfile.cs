﻿using AutoMapper;
using MVFactoryEvaluation.Common.DTOS;
using MVFactoryEvaluation.Enitys;
using System;
using System.Collections.Generic;
using System.Text;

namespace MVFactoryEvaluation.Bussiness.Mapper.Profiles
{
    class AlertProfile : Profile
    {
        public AlertProfile()
        {

            CreateMap<Alert, AlertDTO>().ReverseMap()
                .ForMember(x => x.Cause_Id, m => m.MapFrom(p => p.Cause))
                .ForMember(x => x.Effect_Id, m => m.MapFrom(p => p.Effect))
                .ForMember(x => x.Cause, opt => opt.Ignore())
                .ForMember(x => x.Effect, opt => opt.Ignore())
                .ForMember(x => x.CessetionDate, opt => opt.Ignore())
                 .ForMember(x => x.UpdateTime, opt => opt.Ignore())
                 .ForMember(x => x.CreationDate, opt => opt.Ignore())
                 .ForMember(x => x.Id, opt => opt.Ignore());





        }
    }
}
