﻿using AutoMapper;
using MVFactoryEvaluation.Common.DTOS;
using MVFactoryEvaluation.Enitys;
using System;
using System.Collections.Generic;
using System.Text;

namespace MVFactoryEvaluation.Bussiness.Mapper.Profiles
{
    class LineProfile : Profile
    {
        public LineProfile()
        {
            CreateMap<LineDTO, Line>() 
                  .ForMember(x => x.Route_Id, opt => opt.MapFrom(m=> m.Route_Id))
                  .ForMember(x => x.Direction_Id, opt => opt.MapFrom(m=> m.Direction_Id))
                  .ForMember(x => x.Direction_Description, opt => opt.MapFrom(m=> m.Direction_Description))
                 .ForMember(x => x.CessetionDate, opt => opt.Ignore())
                 .ForMember(x => x.UpdateTime, opt => opt.Ignore())
                 .ForMember(x => x.CreationDate, opt => opt.Ignore())
                 .ForMember(x => x.Id, opt => opt.Ignore()).ReverseMap();


            CreateMap<LineDTO, Line>().ReverseMap()
                 .ForMember(x => x.Trip_Id, opt => opt.Ignore())
                 .ForMember(x => x.Start_date, opt => opt.Ignore())
                 .ForMember(x => x.Start_date, opt => opt.Ignore());



        }
    }
}
