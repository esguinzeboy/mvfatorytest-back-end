﻿using AutoMapper;
using MVFactoryEvaluation.Bussiness.Mapper.Profiles;
using System;
using System.Collections.Generic;
using System.Text;

namespace MVFactoryEvaluation.Bussiness.Mapper
{
    public class AutoMapperInitializer
    {

        public static IMapper MapperConfiguration()
        {
            var config = new AutoMapper.MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new Active_PeriodProfile());
                cfg.AddProfile(new AlertProfile());
                cfg.AddProfile(new DescriptionTextProfile());
                cfg.AddProfile(new HeaderTextProfile());
                cfg.AddProfile(new Informed_EntityProfile());
                cfg.AddProfile(new LineProfile());
                cfg.AddProfile(new StationProfile());
            });

            return config.CreateMapper();
        }
    }
}
