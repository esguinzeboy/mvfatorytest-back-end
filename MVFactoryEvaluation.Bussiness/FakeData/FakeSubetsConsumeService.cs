﻿using AutoMapper;
using Microsoft.Extensions.Configuration;
using MVFactoryEvaluation.Bussiness.Abstract;
using MVFactoryEvaluation.Bussiness.FakeData;
using MVFactoryEvaluation.Bussiness.Helpers;
using MVFactoryEvaluation.Common.DTOS;
using MVFactoryEvaluation.DataAccesObject.Abstract;
using MVFactoryEvaluation.Enitys;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Web;

namespace MVFactoryEvaluation.Bussiness.FakeData
{
    public class FakeSubetsConsumeService : ISubtesConsumeService
    {
        IConfiguration Configuration;
        // IServiceAlert serviceAlert;
        string BaseUrl;
        double TimeoutForRequest;
        IAlertRepository AlertRepository;
        IMapper Mapper;


        public FakeSubetsConsumeService(IConfiguration _configuration, IAlertRepository alertRepository, IMapper mapper)
        {
            Configuration = _configuration;
            BaseUrl = Configuration["ClientHanler:BaseUrl"];
            TimeoutForRequest = double.Parse(Configuration["ClientHanler:TimeOut"]);
            AlertRepository = alertRepository;
            Mapper = mapper;
        }
        /// <summary>
        /// Same service as SubtesConsumeService but get fakeData
        /// </summary>
        /// <returns></returns>
        public ResponseDTO<List<LineDTO>> GetActualTrip()
        {
            ResponseDTO<List<LineDTO>> responseDTO = new ResponseDTO<List<LineDTO>>();

            responseDTO.StatusCode = 200;
            var jsonstring = FakeTrips.FakeAlertsArray();

            JsonHandler<LineDTO> jsonHandler = new JsonHandler<LineDTO>();
            var listOfLines = jsonHandler.DeserializerListTrips(jsonstring);
            responseDTO.DtoToReturn = listOfLines;
            responseDTO.Messege = "Obtencion De Datos OK";


            return responseDTO;

        }

        public ResponseDTO<List<AlertDTO>> GetAlert()
        {
            ResponseDTO<List<AlertDTO>> responseDTO = new ResponseDTO<List<AlertDTO>>();


             var jsonstring = FakeAlerts.FakeAlertsArray();
            responseDTO.StatusCode = 200;

            JsonHandler<AlertDTO> jsonHandler = new JsonHandler<AlertDTO>();
            var listOfAlerts = jsonHandler.DeserializerListAlerts(jsonstring);
            responseDTO.DtoToReturn = listOfAlerts;
            responseDTO.Messege = "ObtencionDeDatosOK";

            if (listOfAlerts.Count > 0)
            {
                SaveAlerts(listOfAlerts);
           

            }
            
            return responseDTO;

        }

        private bool SaveAlerts(List<AlertDTO> listOfAlerts)
        {
      
            var ListOfAlertsEntitys = Mapper.Map<List<AlertDTO>, List<Alert>>(listOfAlerts);
            var validateExistence = ValidatExistenceOFAlertInDB(ListOfAlertsEntitys);

            if (validateExistence.Count > 0)
            {
                var creationDate = DateTime.Today;
                foreach (var item in validateExistence)
                {
                    var AlertId = Guid.NewGuid();
                    item.Id = AlertId;
                    if (item.Active_Periods != null)
                        foreach (var ActivePeriod in item.Active_Periods)
                        {
                            ActivePeriod.Id = Guid.NewGuid();
                            ActivePeriod.Alert_Id = AlertId;
                            ActivePeriod.CreationDate = creationDate;
                        };
                    if (item.Informed_Entities != null)
                        foreach (var InformedEntitie in item.Informed_Entities)
                        {
                            InformedEntitie.Id = Guid.NewGuid();
                            InformedEntitie.Alert_Id = AlertId;
                            InformedEntitie.CreationDate = creationDate;
                        };
                    if (item.Description_Texts != null)
                        foreach (var DescriptionTexts in item.Description_Texts)
                        {
                            DescriptionTexts.Id = Guid.NewGuid();
                            DescriptionTexts.Alert_Id = AlertId;
                            //  DescriptionTexts.isHeader = false;
                            DescriptionTexts.CreationDate = creationDate;

                        };

                    if (item.Header_Texts.Count > 0)
                        foreach (var HeaderTexts in item.Header_Texts)
                        {
                            HeaderTexts.Id = Guid.NewGuid();
                            HeaderTexts.Alert_Id = AlertId;
                            //HeaderTexts.isHeader = true;
                            HeaderTexts.CreationDate = creationDate;

                        };
                }


                AlertRepository.CreateRange(ListOfAlertsEntitys);
                AlertRepository.Save();
            }
            return true;
        }

        private List<Alert> ValidatExistenceOFAlertInDB(List<Alert> list)
        {
            List<Alert> newAlerts = new List<Alert>();

            foreach (var item in list)
            {
                var exist = AlertRepository.AlertExist(item);
                if (!exist)
                    newAlerts.Add(item);
            }

            return newAlerts;


        }



    }
}
