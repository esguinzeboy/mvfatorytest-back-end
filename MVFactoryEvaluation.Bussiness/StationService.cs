﻿using Microsoft.Extensions.Configuration;
using MVFactoryEvaluation.Bussiness.Abstract;
using MVFactoryEvaluation.Common.DTOS;
using MVFactoryEvaluation.DataAccesObject.Abstract;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Web;

namespace MVFactoryEvaluation.Bussiness
{
    public class StationService : IStationService
    {
        IConfiguration Configuration;
        IStationRepository StationRepository;


        

        public StationService(IConfiguration _configuration, IStationRepository _StationRepository)
        {
            Configuration = _configuration;
            StationRepository = _StationRepository;
           
        }

        ResponseDTO<List<StationDTO>> IStationService.GetAll()
        {
            ResponseDTO<List<StationDTO>> responseDto = new ResponseDTO<List<StationDTO>>();
            List<StationDTO> StationsDtos = new List<StationDTO>();
            var ListOfStations = StationRepository.GetAll();
            foreach (var item in ListOfStations)
            {
                StationsDtos.Add(new StationDTO
                {
                      Stop_id = item.Stop_id,
                     Stop_name = item.Stop_name
                });
            }
            responseDto.DtoToReturn = StationsDtos;
            responseDto.StatusCode = 200;
            responseDto.Messege = "resultado OK";
            return responseDto;
        }

        ResponseDTO<StationDTO> IStationService.GetLineById()
        {
            throw new NotImplementedException();
        }
    }
}
