﻿using MVFactoryEvaluation.Common.DTOS;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace MVFactoryEvaluation.Bussiness.Abstract
{
    public interface ILineService
    {
        ResponseDTO<List<LineDTO>> GetAll();
        List<LineDTO> GetAllFull();
        ResponseDTO<LineDTO> GetLineById();

    }
}
