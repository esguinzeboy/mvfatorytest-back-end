﻿using MVFactoryEvaluation.Common.DTOS;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace MVFactoryEvaluation.Bussiness.Abstract
{
    public interface ISubtesConsumeService
    {
        ResponseDTO<List<LineDTO>> GetActualTrip();
        ResponseDTO<List<AlertDTO>> GetAlert();

    }
}
