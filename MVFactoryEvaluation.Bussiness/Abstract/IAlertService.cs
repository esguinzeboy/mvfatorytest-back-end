﻿using MVFactoryEvaluation.Common.DTOS;
using System.Collections.Generic;

namespace MVFactoryEvaluation.Bussiness.Abstract
{
    public interface IAlertService
    {
        ResponseDTO<List<AlertDTO>> GetAll();
        ResponseDTO<AlertDTO> GetLineById();

    }
}
