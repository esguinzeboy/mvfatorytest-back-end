﻿using MVFactoryEvaluation.Common.DTOS;
using System.Collections.Generic;

namespace MVFactoryEvaluation.Bussiness.Abstract
{
    public interface ICauseService
    {
        ResponseDTO<List<CauseDTO>> GetAll();
        ResponseDTO<CauseDTO> GetLineById();

    }
}
