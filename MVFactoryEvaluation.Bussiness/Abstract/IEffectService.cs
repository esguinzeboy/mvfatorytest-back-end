﻿using MVFactoryEvaluation.Common.DTOS;
using System.Collections.Generic;

namespace MVFactoryEvaluation.Bussiness.Abstract
{
    public interface IEffectService
    {
        ResponseDTO<List<EffectDTO>> GetAll();
        ResponseDTO<EffectDTO> GetLineById();

    }
}
