﻿using MVFactoryEvaluation.Common.DTOS;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace MVFactoryEvaluation.Bussiness.Helpers
{
    public class JsonHandler<T> where T : class
    {
        public List<T> DeserializerListTrips(string jsonString)
        {
            JObject entitys = JObject.Parse(jsonString);
            var father = entitys["Entity"].Children();
            var son = father["Linea"];

            List<T> listToReturn = new List<T>();
         
            foreach (JToken result in son)
            {
                T searchResult = result.ToObject<T>();
                listToReturn.Add(searchResult);
            }
            return listToReturn;
                
        }


        public T Deserializer(string jsonString)
        {
            T objetToReturn = Newtonsoft.Json.JsonConvert.DeserializeObject<T>(jsonString);
            return objetToReturn;
        }

        public List<T> DeserializerListAlerts(string jsonString)
        {
            JObject entitys = JObject.Parse(jsonString);
            var father = entitys["entity"].Children();
           // var son = father["Linea"];

            List<T> listToReturn = new List<T>();

            foreach (JToken result in father)
            {
                T searchResult = result.ToObject<T>();
                listToReturn.Add(searchResult);
            }
            return listToReturn;

        }



    }
}
