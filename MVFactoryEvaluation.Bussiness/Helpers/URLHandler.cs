﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.Web;

namespace MVFactoryEvaluation.Bussiness.Helpers
{
    public static class URLHandler
    {  
    /// <summary>
    /// Allow format querystring for request
    /// </summary>
    /// <param name="baseUrl">Base Url without /</param>
    /// <param name="controllerMethod">/controller/method</param>
    /// <param name="parametes">name and value of parameter</param>
    /// <returns></returns>
        public static string UrlFormater(string baseUrl, string controllerMethod, Dictionary<string,string> parametes)
        {

            var uriBuilder = BaseContructURL(baseUrl, controllerMethod);

            var query = HttpUtility.ParseQueryString(uriBuilder.Query);
             
            foreach (var item in parametes)
            {
                query[item.Key] = item.Value;
            }
            
            uriBuilder.Query = query.ToString();
            return uriBuilder.ToString();

        }

        /// <summary>
        /// Allow format querystring for request
        /// </summary>
        /// <param name="baseUrl">Base Url without /</param>
        /// <param name="controllerMethod">/controller/method</param>
        /// <param name="parameter">parameter witout definition</param>
        /// <returns></returns>
        public static string UrlFormater(string baseUrl, string controllerMethod, string parameter)=> $"{baseUrl}{controllerMethod}/{parameter}";
        /// <summary>
        /// Allow format querystring for request
        /// </summary>
        /// <param name="baseUrl">Base Url without / </param>
        /// <param name="controllerMethod">/controller/method</param>
        /// <returns></returns>
        public static string UrlFormater(string baseUrl, string controllerMethod) => $"{baseUrl}{controllerMethod}";

        private static UriBuilder BaseContructURL(string baseUrl, string controllerMethod)
        {
            var uri = $"{baseUrl}{controllerMethod}";

            var uriBuilder = new UriBuilder(uri);
            uriBuilder.Port = -1;
            var query = HttpUtility.ParseQueryString(uriBuilder.Query);
            return uriBuilder;
        }



    }
}
