﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace MVFactoryEvaluation.Bussiness.Helpers
{   
    public class ClientHandler
    {

        private double _secsToTimeout;
        // private readonly string _url;

        /// <summary>
        /// 
        /// Handler all comunication with exteran API( GCBA )
        /// </summary>
        /// <param name="secTimeOut">set timeOut</param>
        public ClientHandler(/*string wsUrl,*/ double secTimeOut)
        {
            //ValidateUrl(wsUrl);

            // this._url = wsUrl;
            this._secsToTimeout = secTimeOut;

        }

        private void ValidateUrl(string wsUrl)
        {
            if (String.IsNullOrEmpty(wsUrl))
            {
                throw new Exception("Debe indicarse una URL válida para el cliente.");
            }
        }
        /// <returns></returns>
        public HttpClient CreateClient()
        {
            var client = new HttpClient();
            // client.BaseAddress = new Uri(this._url);

            client.Timeout = TimeSpan.FromSeconds(_secsToTimeout);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            return client;
        }
        /// <summary>
        ///Get method for external consumption
        /// </summary>
        /// <param name="_url">Final Url to consume</param>
        /// <returns></returns>
        public HttpResponseMessage Get(string _url)
        {
            ValidateUrl(_url);

            using (var httpClient = CreateClient())
            {
                return  httpClient.GetAsync(_url).Result;
                
            }

        }

    }
}
