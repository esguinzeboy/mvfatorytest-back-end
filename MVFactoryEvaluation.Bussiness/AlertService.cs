﻿using AutoMapper;
using Microsoft.Extensions.Configuration;
using MVFactoryEvaluation.Bussiness.Abstract;
using MVFactoryEvaluation.Common.DTOS;
using MVFactoryEvaluation.DataAccesObject.Abstract;
using MVFactoryEvaluation.Enitys;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Web;

namespace MVFactoryEvaluation.Bussiness
{
    public class AlertService : IAlertService
    { 
       
        IConfiguration Configuration;
        IAlertRepository AlertRepository;
        IMapper Mapper;

        public AlertService(IConfiguration _configuration, IAlertRepository _AlertRepository,IMapper _mapper)
        {
            Configuration = _configuration;
             AlertRepository = _AlertRepository;
            Mapper = _mapper;
           
        }

        public ResponseDTO<List<AlertDTO>> GetAll()
        {
            ResponseDTO<List<AlertDTO>> responseDto = new ResponseDTO<List<AlertDTO>>();
            var listOfAlert = AlertRepository.GetWithAllChildresn();

            var AlertDtos = Mapper.Map<List<Alert>, List<AlertDTO>>(listOfAlert);

            responseDto.DtoToReturn = AlertDtos;
            responseDto.StatusCode = 200;
            responseDto.Messege = "resultado OK";
            return responseDto;

        }

        public ResponseDTO<AlertDTO> GetLineById()
        {
            throw new NotImplementedException();
        }


    }
}
