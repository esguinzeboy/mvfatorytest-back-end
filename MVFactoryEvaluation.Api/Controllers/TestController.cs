﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVFactoryEvaluation.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class TestController : ControllerBase
    {
        /// <summary>
        /// use for testing the app
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetAll()
        {

            var algo = TimeZoneInfo.Local.GetUtcOffset(DateTime.UtcNow);

            return Ok("La aplicacion esta funcionando al menos el controlador :)");
        }
    }
}
