﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MVFactoryEvaluation.Bussiness.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVFactoryEvaluation.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class EffectController : ControllerBase
    {

        IEffectService EffectService;
        ILogger _Logger;

        public EffectController(IEffectService _EffectService, ILogger<EffectController> logger)
        {
            EffectService = _EffectService;
            this._Logger = logger;
        }
        /// <summary>
        /// get Effect Entitys
        /// </summary>
        /// <returns></returns>
        [HttpGet()]
        public IActionResult GetAll()
        {
            try
            {
                var result = EffectService.GetAll();
                if (result.StatusCode == 200)
                {
                    return Ok(result.DtoToReturn);
                }
                return StatusCode(500, "Error Interno");

            }
            catch (Exception ex)
            {
                _Logger.LogError(ex, "error cacheado en controlador");
                return StatusCode(500, "Error Interno");
            }

           
            
        }


    }
}
