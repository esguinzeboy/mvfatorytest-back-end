﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MVFactoryEvaluation.Bussiness.Abstract;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVFactoryEvaluation.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class CauseController : ControllerBase
    {

        ICauseService CauseService;
        ILogger _Logger;

        public CauseController(ICauseService _causeService, ILogger<CauseController> Logger)
        {
            CauseService = _causeService;
            this._Logger = Logger;
        }

        /// <summary>
        /// Get cause entytys
        /// </summary>
        /// <returns></returns>
        [HttpGet()]
        public IActionResult GetAll()
        {
            try
            {
                var result = CauseService.GetAll();
                if (result.StatusCode == 200)
                {
                    return Ok(result.DtoToReturn);
                }
                return StatusCode(500, "Error Interno");
            }
            catch (Exception ex)
            {
                _Logger.LogError(ex, "error cacheado en controlador");
                return StatusCode(500, "Error Interno");
            }
          

        }


    }
}
