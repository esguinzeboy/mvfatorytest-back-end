﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MVFactoryEvaluation.Bussiness.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVFactoryEvaluation.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class SubteExtController : ControllerBase
    {

        ISubtesConsumeService SubtesConsumeService;

        ILogger _Logger; 

        public SubteExtController(ISubtesConsumeService _subtesConsumeService, ILogger<StationController> logger)
        {
            SubtesConsumeService = _subtesConsumeService;
            _Logger = logger;
        }


        /// <summary>
        /// Get trips from extrernal api GCBA and reorder the json
        /// </summary>
        /// <returns></returns>
        [HttpGet("trips")]
        public IActionResult GetTrips()
        {
            try
            {
                var result = SubtesConsumeService.GetActualTrip();
                if (result.StatusCode == 200)
                {
                    return Ok(result.DtoToReturn);
                }

                if (result.StatusCode == 0)
                {
                    result.StatusCode = 500;
                    return StatusCode(result.StatusCode, "Error Interno");

                }
                return StatusCode(200, $"La api externa devolvio un error= {result.StatusCode}");
            }
            catch (Exception ex)
            {
                _Logger.LogError(ex, "error cacheado en controlador");
                return StatusCode(500, "Error Interno");
            }
           
            
        }

        /// <summary>
        /// Get Alertes from external api of CCBA and reorder the json for display
        /// </summary>
        /// <returns></returns>
        [HttpGet("alerts")]
        public IActionResult GetAlerts()
        {
            try
            {
                var result = SubtesConsumeService.GetAlert();
                if (result.StatusCode == 200)
                {
                    if (result.DtoToReturn.Count > 0)
                    return Ok(result.DtoToReturn);

                    return StatusCode(200, "no hay alertas");
                }
                if (result.StatusCode == 0)
                {
                    result.StatusCode = 500;
                    return StatusCode(result.StatusCode, "Error Interno");

                }
                return StatusCode(200, $"La api externa devolvio un error= {result.StatusCode}");

            }
            catch (Exception ex)
            {
                _Logger.LogError(ex, "error cacheado en controlador");
                return StatusCode(500, "Error Interno");
            }
            
            //  return Content(result.StatusCode.ToString(), "ocurrio un error");

        }

    }
}
