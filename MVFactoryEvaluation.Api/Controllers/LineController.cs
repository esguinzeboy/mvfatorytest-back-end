﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MVFactoryEvaluation.Bussiness.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVFactoryEvaluation.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class LineController : ControllerBase
    {

        ILineService LineService;
        ILogger _Logger;

        public LineController(ILineService _LineService, ILogger<LineController> logger)
        {
            LineService = _LineService;
            _Logger = logger;
        }

        
        /// <summary>
        /// Get all lines but not entiere entity
        /// </summary>
        /// <returns></returns>
        [HttpGet()]
        public IActionResult GetAll()
        {
            try
            {
                var result = LineService.GetAll();
                if (result.StatusCode == 200)
                {
                    return Ok(result.DtoToReturn);
                }
                return StatusCode(500, "Error Interno");

            }

            catch (Exception ex)
            {
                _Logger.LogError(ex, "error cacheado en controlador");
                return StatusCode(500, "se produjo un error");
            }

        }
        
        //TODO Revisar 
        /// <summary>
        /// Get all Lines entire entityt
        /// </summary>
        /// <returns></returns>
        [HttpGet("Full")]
        public IActionResult GetAllFull()
        {
            try
            {
                var result = LineService.GetAllFull();
               if (result.Count > 0)
                return Ok(result);
               
                return StatusCode(500, "Error Interno");

            }

            catch (Exception ex)
            {
                _Logger.LogError(ex, "error cacheado en controlador");
                return StatusCode(500, "se produjo un error");
            }

        }


    }
}
