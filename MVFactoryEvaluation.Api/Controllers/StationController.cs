﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MVFactoryEvaluation.Bussiness.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVFactoryEvaluation.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class StationController : ControllerBase
    {

        IStationService StationService;
        ILogger _Logger { get; }

        public StationController(IStationService _StationService, ILogger<StationController> logger)
        {
            StationService = _StationService;
            _Logger = logger;
        }

       /// <summary>
       /// Get station entity
       /// </summary>
       /// <returns></returns>

        [HttpGet()]
        public IActionResult GetAll()
        {
            try
            {
                var result = StationService.GetAll();
                if (result.StatusCode == 200)
                {
                    return Ok(result.DtoToReturn);
                }
                return StatusCode(500, "Error Interno");

            }
            catch (Exception ex)
            {
                _Logger.LogError(ex, "error cacheado en controlador");

                return StatusCode(500, "Error Interno"); 
            }

          

        }


    }
}
