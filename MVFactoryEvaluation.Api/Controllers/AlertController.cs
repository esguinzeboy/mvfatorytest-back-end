﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MVFactoryEvaluation.Bussiness.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVFactoryEvaluation.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class AlertController : ControllerBase
    {

        IAlertService AlertService;
       ILogger _Logger;

        public AlertController(IAlertService _AlertService, ILogger<AlertController> _logger )
        {
            AlertService = _AlertService;
            _Logger = _logger;
        }
        /// <summary>
        /// Get all alert Service
        /// </summary>
        /// <returns></returns>
        [HttpGet()]
        public IActionResult GetAll()
        {
            try
            {
              
                var result = AlertService.GetAll();
                if (result.StatusCode == 200)
                {
                    return Ok(result.DtoToReturn);
                }
                return StatusCode(500, "Error Interno");
                
            }
            catch (Exception ex)
            {
                _Logger.LogError(ex,"error cacheado en controlador");
                return StatusCode(500, "Error Interno");
                
            }
          

        }


    }
}
