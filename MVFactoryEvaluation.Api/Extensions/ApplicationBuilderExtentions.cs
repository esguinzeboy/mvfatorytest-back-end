﻿using MVFactoryEvaluation.Bussiness.Mapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVFactoryEvaluation.Extensions
{
    public class ApplicationBuilderExtentions
    {

        public static AutoMapperInitializer AutoMapperInitializer { get; set; }
    }
}
