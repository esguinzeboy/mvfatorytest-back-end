﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using MVFactoryEvaluation.Bussiness;
using MVFactoryEvaluation.Bussiness.Abstract;
using MVFactoryEvaluation.Bussiness.FakeData;
using MVFactoryEvaluation.Bussiness.Mapper;
using MVFactoryEvaluation.DataAccesObject;
using MVFactoryEvaluation.DataAccesObject.Abstract;
using MVFactoryEvaluation.DataAccessLayer;
using MVFactoryEvaluation.DataAccessLayer.Abstract;
using Swashbuckle.AspNetCore.Swagger;

namespace MVFactoryEvaluation.Api
{
    public class Startup
    {

        public IConfiguration Configuration { get; }
        public ILogger logger;

        public Startup(IConfiguration configuration,ILogger<Startup> logger)
        {
            Configuration = configuration;
            this.logger = logger;
        }



        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddMvc().AddJsonOptions(options =>
            {
                //options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                options.SerializerSettings.DateTimeZoneHandling = Newtonsoft.Json.DateTimeZoneHandling.Local;

            });


            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "MVFactoryEvaluation",
                    Description = "Test for MVFactoryEvaluation",
                    TermsOfService = "None",
                });
            });

            
            var connectionString = Configuration["connectionStrings:DBConnection"];

            ManagerContext managerContext = new ManagerContext(connectionString);

            services.AddScoped<IManagerContextInstanciator, ManagerContextInstanciator>(serviceProvider =>
            {

                return new ManagerContextInstanciator(managerContext);
            });


            services.AddCors(c =>
            {
                c.AddPolicy("AllowOrigin", options => options.AllowAnyOrigin());
            });

            services.AddSingleton<IMapper>(AutoMapperInitializer.MapperConfiguration());

            //Instancio Respository

            services.AddScoped<ILineRepository, LineRepository>();
            services.AddScoped<IStationRepository, StationRepository>();
            services.AddScoped<IAlertRepository, AlertRepository>();
            services.AddScoped<ICauseRepository, CauseRepository>();
            services.AddScoped<IEffectRepository, EffectRepository>();

            // Instancio Servicio  
             //services.AddScoped<ISubtesConsumeService, SubetsConsumeService>();
            services.AddScoped<ISubtesConsumeService, FakeSubetsConsumeService>();

            services.AddScoped<ILineService, LineService>();
            services.AddScoped<IStationService, StationService>();
            services.AddScoped<ICauseService, CauseService>();
            services.AddScoped<IEffectService, EffectService>();
            services.AddScoped<IAlertService, AlertService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            // app.UseAutoMapperConfigurations();
            app.UseHttpsRedirection();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");

                // c.RoutePrefix = string.Empty;
            });

            app.UseCors(options => options.AllowAnyOrigin());



            app.UseMvc();
        }
    }
}
