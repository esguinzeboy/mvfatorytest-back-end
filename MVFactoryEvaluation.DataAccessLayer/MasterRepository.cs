﻿using Microsoft.EntityFrameworkCore;
using MVFactoryEvaluation.DataAccessLayer.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MVFactoryEvaluation.DataAccessLayer
{
    public class MasterRepository<TEntity, TKey> : IMasterRepository<TEntity, TKey> where TEntity : class
    {
        private readonly DbSet<TEntity> _dbSet;

        public DbSet<TEntity> DbSet { get { return _dbSet; } }

        public static IManagerContextInstanciator _context;

        public MasterRepository(IManagerContextInstanciator ManagerContext)
        {
            _context = ManagerContext ?? throw new ArgumentNullException(nameof(ManagerContext), @"Unit of work cannot be null");
            _dbSet = ManagerContext.ManagerContext.Set<TEntity>();
        }

        public bool Any(Expression<Func<TEntity, bool>> predicate)
        {
            return _dbSet.Any(predicate);
        }

        public void Create(ref TEntity entity)
        {
            _dbSet.Add(entity);
        }
        public void CreateRange(List<TEntity> listEntiys)
        {
            _dbSet.AddRange(listEntiys);

        }


        public void DeleteFisic(TEntity entity)
        {
            _dbSet.Remove(entity);
        }

        public void DeleteLogic(TEntity entity)
        {
            throw new NotImplementedException();
        }

        public TEntity Get(TKey id)
        {
            return _dbSet.Find(id);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return _dbSet.ToList();
        }

        public TEntity OrderByDescAndFirstEntity<TQuery>(Expression<Func<TEntity, TQuery>> predicate)
        {
            return _dbSet.OrderByDescending(predicate).FirstOrDefault();
        }

        public IQueryable<TEntity> Queryable(Expression<Func<TEntity, bool>> predicate)
        {
            return _dbSet.Where(predicate);
        }

        public bool Save()
        {
            _context.ManagerContext.SaveChanges();
            return true;
        }

        public TEntity SingleOrDefault(Expression<Func<TEntity, bool>> predicate)
        {
            return _dbSet.Where(predicate).SingleOrDefault();
        }


    }
}
