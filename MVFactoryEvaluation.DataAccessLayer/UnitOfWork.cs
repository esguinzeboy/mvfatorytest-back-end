﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using MVFactoryEvaluation.DataAccessLayer.Abstract;
using MVFactoryEvaluation.Enitys;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MVFactoryEvaluation.DataAccessLayer
{
    class UnitOfWork : IUnitOfWork
    {
       public ManagerContext ManagerContext { get; }
       public IMasterRepository<Line, Guid> LineRepository { get; private set; }
       public IMasterRepository<Station, Guid> StationRepository { get; private set; }

        public UnitOfWork(IManagerContextInstanciator _IManagerContextInstanciator)
        {

            ManagerContext = _IManagerContextInstanciator.ManagerContext;
            // Repository Generic
            LineRepository = new MasterRepository<Line, Guid>(_IManagerContextInstanciator);
            StationRepository = new MasterRepository<Station, Guid>(_IManagerContextInstanciator);


        }

        public void Dispose()
        {
            ManagerContext.Dispose();
            GC.SuppressFinalize(this);
        }

        public void Save()
        {
            ManagerContext.SaveChanges();
        }
    }
}
