﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MVFactoryEvaluation.DataAccessLayer.Migrations
{
    public partial class chanelineandstationtables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SetStation_SetLine_LineId",
                table: "SetStation");

            migrationBuilder.DropPrimaryKey(
                name: "PK_SetStation",
                table: "SetStation");

            migrationBuilder.DropPrimaryKey(
                name: "PK_SetLine",
                table: "SetLine");

            migrationBuilder.RenameTable(
                name: "SetStation",
                newName: "Station");

            migrationBuilder.RenameTable(
                name: "SetLine",
                newName: "Line");

            migrationBuilder.RenameIndex(
                name: "IX_SetStation_LineId",
                table: "Station",
                newName: "IX_Station_LineId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Station",
                table: "Station",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Line",
                table: "Line",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Station_Line_LineId",
                table: "Station",
                column: "LineId",
                principalTable: "Line",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Station_Line_LineId",
                table: "Station");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Station",
                table: "Station");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Line",
                table: "Line");

            migrationBuilder.RenameTable(
                name: "Station",
                newName: "SetStation");

            migrationBuilder.RenameTable(
                name: "Line",
                newName: "SetLine");

            migrationBuilder.RenameIndex(
                name: "IX_Station_LineId",
                table: "SetStation",
                newName: "IX_SetStation_LineId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_SetStation",
                table: "SetStation",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_SetLine",
                table: "SetLine",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_SetStation_SetLine_LineId",
                table: "SetStation",
                column: "LineId",
                principalTable: "SetLine",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
