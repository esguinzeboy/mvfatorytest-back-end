﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MVFactoryEvaluation.DataAccessLayer.Migrations
{
    public partial class ini : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Cause",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    UpdateTime = table.Column<DateTime>(nullable: true),
                    CessetionDate = table.Column<DateTime>(nullable: true),
                    Cause_id = table.Column<int>(nullable: false),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cause", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Effect",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    UpdateTime = table.Column<DateTime>(nullable: true),
                    CessetionDate = table.Column<DateTime>(nullable: true),
                    Effect_id = table.Column<int>(nullable: false),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Effect", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SetLine",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    UpdateTime = table.Column<DateTime>(nullable: true),
                    CessetionDate = table.Column<DateTime>(nullable: true),
                    Route_Id = table.Column<string>(nullable: true),
                    Direction_Id = table.Column<int>(nullable: false),
                    Direction_Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SetLine", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Alert",
                columns: table => new
                {
                    CessetionDate = table.Column<DateTime>(nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    Id = table.Column<Guid>(nullable: false),
                    UpdateTime = table.Column<DateTime>(nullable: true),
                    Cause_Id = table.Column<int>(nullable: false),
                    CauseId = table.Column<int>(nullable: true),
                    Effect_Id = table.Column<int>(nullable: false),
                    EffectId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Alert", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Alert_Cause_CauseId",
                        column: x => x.CauseId,
                        principalTable: "Cause",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Alert_Effect_EffectId",
                        column: x => x.EffectId,
                        principalTable: "Effect",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SetStation",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    UpdateTime = table.Column<DateTime>(nullable: true),
                    CessetionDate = table.Column<DateTime>(nullable: true),
                    Stop_id = table.Column<string>(nullable: true),
                    Stop_name = table.Column<string>(nullable: true),
                    Line_Id = table.Column<int>(nullable: false),
                    LineId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SetStation", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SetStation_SetLine_LineId",
                        column: x => x.LineId,
                        principalTable: "SetLine",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Active_Period",
                columns: table => new
                {
                    CessetionDate = table.Column<DateTime>(nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    Id = table.Column<Guid>(nullable: false),
                    UpdateTime = table.Column<DateTime>(nullable: true),
                    Start = table.Column<DateTime>(nullable: false),
                    End = table.Column<DateTime>(nullable: false),
                    Alert_Id = table.Column<Guid>(nullable: false),
                    AlertId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Active_Period", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Active_Period_Alert_AlertId",
                        column: x => x.AlertId,
                        principalTable: "Alert",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Description_Text",
                columns: table => new
                {
                    CessetionDate = table.Column<DateTime>(nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    Id = table.Column<Guid>(nullable: false),
                    UpdateTime = table.Column<DateTime>(nullable: true),
                    Text = table.Column<string>(nullable: true),
                    Language = table.Column<string>(nullable: true),
                    Alert_Id = table.Column<Guid>(nullable: false),
                    AlertId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Description_Text", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Description_Text_Alert_AlertId",
                        column: x => x.AlertId,
                        principalTable: "Alert",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Header_Text",
                columns: table => new
                {
                    CessetionDate = table.Column<DateTime>(nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    Id = table.Column<Guid>(nullable: false),
                    UpdateTime = table.Column<DateTime>(nullable: true),
                    Text = table.Column<string>(nullable: true),
                    Language = table.Column<string>(nullable: true),
                    Alert_Id = table.Column<Guid>(nullable: false),
                    AlertId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Header_Text", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Header_Text_Alert_AlertId",
                        column: x => x.AlertId,
                        principalTable: "Alert",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Informed_entity",
                columns: table => new
                {
                    CessetionDate = table.Column<DateTime>(nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    Id = table.Column<Guid>(nullable: false),
                    UpdateTime = table.Column<DateTime>(nullable: true),
                    Agency_Id = table.Column<string>(nullable: true),
                    Rout_Id = table.Column<string>(nullable: true),
                    Route_Type = table.Column<int>(nullable: false),
                    Stop_Id = table.Column<string>(nullable: true),
                    Alert_Id = table.Column<Guid>(nullable: false),
                    AlertId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Informed_entity", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Informed_entity_Alert_AlertId",
                        column: x => x.AlertId,
                        principalTable: "Alert",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "Cause",
                columns: new[] { "Id", "Cause_id", "CessetionDate", "CreationDate", "Description", "UpdateTime" },
                values: new object[,]
                {
                    { 1, 1, null, new DateTime(2019, 8, 5, 0, 0, 0, 0, DateTimeKind.Local), "Causa Desconocida", null },
                    { 2, 2, null, new DateTime(2019, 8, 5, 0, 0, 0, 0, DateTimeKind.Local), "Otra Causa", null },
                    { 3, 3, null, new DateTime(2019, 8, 5, 0, 0, 0, 0, DateTimeKind.Local), "Problemas Tecnicos", null },
                    { 4, 4, null, new DateTime(2019, 8, 5, 0, 0, 0, 0, DateTimeKind.Local), "Paro", null },
                    { 5, 5, null, new DateTime(2019, 8, 5, 0, 0, 0, 0, DateTimeKind.Local), "Demostracion", null },
                    { 6, 6, null, new DateTime(2019, 8, 5, 0, 0, 0, 0, DateTimeKind.Local), "Accidente", null },
                    { 7, 7, null, new DateTime(2019, 8, 5, 0, 0, 0, 0, DateTimeKind.Local), "Vacaciones", null },
                    { 8, 8, null, new DateTime(2019, 8, 5, 0, 0, 0, 0, DateTimeKind.Local), "Clima", null },
                    { 9, 9, null, new DateTime(2019, 8, 5, 0, 0, 0, 0, DateTimeKind.Local), "Mantenimiento", null },
                    { 10, 10, null, new DateTime(2019, 8, 5, 0, 0, 0, 0, DateTimeKind.Local), "Obras", null },
                    { 11, 11, null, new DateTime(2019, 8, 5, 0, 0, 0, 0, DateTimeKind.Local), "Actividad Policial", null },
                    { 12, 12, null, new DateTime(2019, 8, 5, 0, 0, 0, 0, DateTimeKind.Local), "Emergencia Medica", null }
                });

            migrationBuilder.InsertData(
                table: "Effect",
                columns: new[] { "Id", "CessetionDate", "CreationDate", "Description", "Effect_id", "UpdateTime" },
                values: new object[,]
                {
                    { 9, null, new DateTime(2019, 8, 5, 0, 0, 0, 0, DateTimeKind.Local), "Linea Parada", 9, null },
                    { 8, null, new DateTime(2019, 8, 5, 0, 0, 0, 0, DateTimeKind.Local), "Effecto Desconocido", 8, null },
                    { 6, null, new DateTime(2019, 8, 5, 0, 0, 0, 0, DateTimeKind.Local), "Servicio Modificado", 6, null },
                    { 5, null, new DateTime(2019, 8, 5, 0, 0, 0, 0, DateTimeKind.Local), "Servicio Adicional", 5, null },
                    { 7, null, new DateTime(2019, 8, 5, 0, 0, 0, 0, DateTimeKind.Local), "Otros Effectos", 7, null },
                    { 3, null, new DateTime(2019, 8, 5, 0, 0, 0, 0, DateTimeKind.Local), "Demoras", 3, null },
                    { 2, null, new DateTime(2019, 8, 5, 0, 0, 0, 0, DateTimeKind.Local), "Servicio Reducido", 2, null },
                    { 1, null, new DateTime(2019, 8, 5, 0, 0, 0, 0, DateTimeKind.Local), "Sin Servicio", 1, null },
                    { 4, null, new DateTime(2019, 8, 5, 0, 0, 0, 0, DateTimeKind.Local), "Desvio", 4, null }
                });

            migrationBuilder.InsertData(
                table: "SetLine",
                columns: new[] { "Id", "CessetionDate", "CreationDate", "Direction_Description", "Direction_Id", "Route_Id", "UpdateTime" },
                values: new object[,]
                {
                    { 8, null, new DateTime(2019, 8, 5, 0, 0, 0, 0, DateTimeKind.Local), "San Pedrito - Plaza de Mayo", 0, "LineaE", null },
                    { 7, null, new DateTime(2019, 8, 5, 0, 0, 0, 0, DateTimeKind.Local), "San Pedrito - Plaza de Mayo", 1, "LineaE", null },
                    { 5, null, new DateTime(2019, 8, 5, 0, 0, 0, 0, DateTimeKind.Local), "Congreso de Tucumán - Catedral", 1, "LineaD", null },
                    { 6, null, new DateTime(2019, 8, 5, 0, 0, 0, 0, DateTimeKind.Local), "Catedral -Congreso de Tucumán", 0, "LineaD", null },
                    { 3, null, new DateTime(2019, 8, 5, 0, 0, 0, 0, DateTimeKind.Local), "Rosas - Alem", 1, "LineaB", null },
                    { 2, null, new DateTime(2019, 8, 5, 0, 0, 0, 0, DateTimeKind.Local), "Plaza de Mayo- San Pedrito", 0, "LineaA", null },
                    { 1, null, new DateTime(2019, 8, 5, 0, 0, 0, 0, DateTimeKind.Local), "San Pedrito - Plaza de Mayo", 1, "LineaA", null },
                    { 4, null, new DateTime(2019, 8, 5, 0, 0, 0, 0, DateTimeKind.Local), "Alem - Rosas", 0, "LineaB", null }
                });

            migrationBuilder.InsertData(
                table: "SetStation",
                columns: new[] { "Id", "CessetionDate", "CreationDate", "LineId", "Line_Id", "Stop_id", "Stop_name", "UpdateTime" },
                values: new object[,]
                {
                    { 8, null, new DateTime(2019, 8, 5, 0, 0, 0, 0, DateTimeKind.Local), null, 1, "1065N", "Rio de Janeiro", null },
                    { 1, null, new DateTime(2019, 8, 5, 0, 0, 0, 0, DateTimeKind.Local), null, 1, "1059N", "San Pedrito", null },
                    { 2, null, new DateTime(2019, 8, 5, 0, 0, 0, 0, DateTimeKind.Local), null, 1, "1060N", "San José de Flores", null },
                    { 3, null, new DateTime(2019, 8, 5, 0, 0, 0, 0, DateTimeKind.Local), null, 1, "1061N", "Carabobo", null },
                    { 5, null, new DateTime(2019, 8, 5, 0, 0, 0, 0, DateTimeKind.Local), null, 1, "1062N", "Puan", null },
                    { 6, null, new DateTime(2019, 8, 5, 0, 0, 0, 0, DateTimeKind.Local), null, 1, "1063N", "PrimeraJunta", null },
                    { 7, null, new DateTime(2019, 8, 5, 0, 0, 0, 0, DateTimeKind.Local), null, 1, "1064N", "Acoyte", null },
                    { 9, null, new DateTime(2019, 8, 5, 0, 0, 0, 0, DateTimeKind.Local), null, 1, "1066N", "Castro Barro", null }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Active_Period_AlertId",
                table: "Active_Period",
                column: "AlertId");

            migrationBuilder.CreateIndex(
                name: "IX_Alert_CauseId",
                table: "Alert",
                column: "CauseId");

            migrationBuilder.CreateIndex(
                name: "IX_Alert_EffectId",
                table: "Alert",
                column: "EffectId");

            migrationBuilder.CreateIndex(
                name: "IX_Description_Text_AlertId",
                table: "Description_Text",
                column: "AlertId");

            migrationBuilder.CreateIndex(
                name: "IX_Header_Text_AlertId",
                table: "Header_Text",
                column: "AlertId");

            migrationBuilder.CreateIndex(
                name: "IX_Informed_entity_AlertId",
                table: "Informed_entity",
                column: "AlertId");

            migrationBuilder.CreateIndex(
                name: "IX_SetStation_LineId",
                table: "SetStation",
                column: "LineId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Active_Period");

            migrationBuilder.DropTable(
                name: "Description_Text");

            migrationBuilder.DropTable(
                name: "Header_Text");

            migrationBuilder.DropTable(
                name: "Informed_entity");

            migrationBuilder.DropTable(
                name: "SetStation");

            migrationBuilder.DropTable(
                name: "Alert");

            migrationBuilder.DropTable(
                name: "SetLine");

            migrationBuilder.DropTable(
                name: "Cause");

            migrationBuilder.DropTable(
                name: "Effect");
        }
    }
}
