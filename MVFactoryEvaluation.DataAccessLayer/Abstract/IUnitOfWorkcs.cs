﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using MVFactoryEvaluation.Enitys;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MVFactoryEvaluation.DataAccessLayer.Abstract
{


    
    public interface IUnitOfWork : IDisposable
    {


        ManagerContext ManagerContext { get; }

        IMasterRepository<Line, Guid> LineRepository { get; }
        IMasterRepository<Station, Guid> StationRepository { get; }

        void Save();

    }
}

