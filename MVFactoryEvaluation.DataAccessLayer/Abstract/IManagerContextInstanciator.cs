﻿using Microsoft.EntityFrameworkCore;
using MVFactoryEvaluation.Enitys;
using System;
using System.Collections.Generic;
using System.Text;

namespace MVFactoryEvaluation.DataAccessLayer.Abstract
{
     public interface IManagerContextInstanciator
    {
         ManagerContext ManagerContext { get; }
    }
}
