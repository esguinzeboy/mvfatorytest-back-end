﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MVFactoryEvaluation.DataAccessLayer.Abstract
{

    public interface IMasterRepository<TEntity, in TKey> where TEntity : class
    {

        DbSet<TEntity> DbSet { get; }

        TEntity Get(TKey id);

        bool Any(Expression<Func<TEntity, bool>> predicate);

        TEntity OrderByDescAndFirstEntity<TQuery>(Expression<Func<TEntity, TQuery>> predicate);

        IEnumerable<TEntity> GetAll();

        TEntity SingleOrDefault(Expression<Func<TEntity, bool>> predicate);

        IQueryable<TEntity> Queryable(Expression<Func<TEntity, bool>> predicate);

        void Create(ref TEntity entity);

        void CreateRange(List<TEntity> listEntiys);

        void DeleteLogic(TEntity entity);

        void DeleteFisic(TEntity entity);

        bool Save();
    }
}
