﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using MVFactoryEvaluation.DataAccessLayer.Abstract;
using MVFactoryEvaluation.DataAccessLayer.fluent;
using MVFactoryEvaluation.DataAccessLayer.Seed;
using MVFactoryEvaluation.Enitys;
using System;
using System.Collections.Generic;
using System.Text;

namespace MVFactoryEvaluation.DataAccessLayer
{ 
    public class ManagerContext: DbContext
    {
       
        string ConnectionString;

        public ManagerContext(string _ConnectionString)
        {
            ConnectionString = _ConnectionString;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(ConnectionString, providerOptions => providerOptions.CommandTimeout(60));
            
           

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            new AlertFluent();
            new Informed_entityFluent();
            new LineFluent();
            new StationFluent();
            new Active_PeriodFluent();
            
            new CauseFluent();
            new Description_TextFluent();
            new EffectFluent();

            new LineFluent();
            new StationFluent();

            modelBuilder.Seed();
        }

      //  public DbSet<Line> SetLine { get; set; }
      //  public DbSet<Station> SetStation { get; set; }
      //  public DbSet<Active_Period> SetActive_Period { get; set; }
      //  public DbSet<Alert> StAlert { get; set; }
      //  public DbSet<Cause> SetCause { get; set; }
      //  public DbSet<Description_Text> SetDescription_Text { get; set; }
      //  public DbSet<Effect> SetEffects { get; set; }
      //  public DbSet<Header_Text> SetHeader_Text { get; set; }
      //  public DbSet<Informed_entity> SetInformed_entity { get; set; }


    }
}
