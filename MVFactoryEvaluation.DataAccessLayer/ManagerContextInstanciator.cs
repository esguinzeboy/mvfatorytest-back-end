﻿using Microsoft.EntityFrameworkCore;
using MVFactoryEvaluation.DataAccessLayer.Abstract;
using MVFactoryEvaluation.Enitys;
using System;
using System.Collections.Generic;
using System.Text;

namespace MVFactoryEvaluation.DataAccessLayer
{
     public class ManagerContextInstanciator :  IManagerContextInstanciator
    {

        public ManagerContext ManagerContext { get; }

       public ManagerContextInstanciator(ManagerContext managerContext)
        {

            ManagerContext = managerContext;
        }


    }
}
