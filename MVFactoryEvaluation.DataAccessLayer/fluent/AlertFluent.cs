﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MVFactoryEvaluation.Enitys;
using System;
using System.Collections.Generic;
using System.Text;

namespace MVFactoryEvaluation.DataAccessLayer.fluent
{

    public class AlertFluent : IEntityTypeConfiguration<Alert>
    {
        public void Configure(EntityTypeBuilder<Alert> builder)
        {
            builder.ToTable("Alert", "dbo");

            builder.HasKey(c => c.Id);

            builder.Property(c => c.CreationDate).IsRequired(true);
            builder.Property(c => c.UpdateTime).IsRequired(false);
            builder.Property(c => c.CessetionDate).IsRequired(false);

         //   builder.Ignore(c => c.Header_Texts);

            builder.HasMany<Active_Period>(g => g.Active_Periods)
            .WithOne(s => s.Alert)
           .HasForeignKey(s => s.Alert_Id);
          
            builder.HasMany<Description_Text>(g => g.Description_Texts)
            .WithOne(s => s.Alert)
           .HasForeignKey(s => s.Alert_Id);

            builder.HasMany<Header_Text>(g => g.Header_Texts)
            .WithOne(s => s.Alert)
            .HasForeignKey(s => s.Alert_Id);


            builder.HasMany<Informed_entity>(g => g.Informed_Entities)
            .WithOne(s => s.Alert)
           .HasForeignKey(s => s.Alert_Id);

            builder.HasOne<Cause>(g => g.Cause).WithMany(c => c.Alerts).HasForeignKey(g=> g.Cause_Id);
            builder.HasOne<Effect>(g => g.Effect).WithMany(c => c.Alerts).HasForeignKey(g => g.Effect_Id);
        



        }

    }
}

