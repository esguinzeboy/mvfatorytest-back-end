﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MVFactoryEvaluation.Enitys;
using System;
using System.Collections.Generic;
using System.Text;

namespace MVFactoryEvaluation.DataAccessLayer.fluent
{
    class StationFluent : IEntityTypeConfiguration<Station>
    {
        public void Configure(EntityTypeBuilder<Station> builder)
        {
            builder.ToTable("Station", "dbo");

            builder.HasKey(c => c.Id);

            builder.Property(c => c.CreationDate).IsRequired(true);
            builder.Property(c => c.UpdateTime).IsRequired(false);
            builder.Property(c => c.CessetionDate).IsRequired(false);

            builder.Property(c => c.Line_Id).IsRequired(true).HasMaxLength(10);
            builder.Property(c => c.Stop_id).IsRequired(true).HasMaxLength(150);

            builder.HasOne<Line>(g => g.Line)
            .WithMany(s => s.Stations)
           .HasForeignKey(s => s.Line_Id);

        }

    }
}

