﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MVFactoryEvaluation.Enitys;
using System;
using System.Collections.Generic;
using System.Text;

namespace MVFactoryEvaluation.DataAccessLayer.fluent
{

    public class Active_PeriodFluent : IEntityTypeConfiguration<Active_Period>
    {
        public void Configure(EntityTypeBuilder<Active_Period> builder)
        {
            builder.ToTable("Active_Period", "dbo");

            builder.HasKey(c => c.Id);

            builder.Property(c => c.CreationDate).IsRequired(true);
            builder.Property(c => c.UpdateTime).IsRequired(false);
            builder.Property(c => c.CessetionDate).IsRequired(false);

            // builder.Property(c => c.Trip_Id).IsRequired(true).HasMaxLength(10);
            builder.Property(c => c.Start).IsRequired(true);
            builder.Property(c => c.End).IsRequired(true);
                        
           builder.HasOne<Alert>(g => g.Alert).WithMany(c => c.Active_Periods).HasForeignKey(g=> g.Alert_Id);



        }

    }
}

