﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MVFactoryEvaluation.Enitys;
using System;
using System.Collections.Generic;
using System.Text;

namespace MVFactoryEvaluation.DataAccessLayer.fluent
{

    public class CauseFluent : IEntityTypeConfiguration<Cause>
    {
        public void Configure(EntityTypeBuilder<Cause> builder)
        {
            builder.ToTable("Cause", "dbo");

            builder.HasKey(c => c.Id);

            builder.Property(c => c.CreationDate).IsRequired(true);
            builder.Property(c => c.UpdateTime).IsRequired(false);
            builder.Property(c => c.CessetionDate).IsRequired(false);

            // builder.Property(c => c.Trip_Id).IsRequired(true).HasMaxLength(10);
            builder.Property(c => c.Cause_id).IsRequired(true);
            builder.Property(c => c.Description).IsRequired(true).HasMaxLength(50);
                        
           builder.HasMany<Alert>(g => g.Alerts).WithOne(c => c.Cause).HasForeignKey(g=> g.Cause_Id);



        }

    }
}

