﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MVFactoryEvaluation.Enitys;
using System;
using System.Collections.Generic;
using System.Text;

namespace MVFactoryEvaluation.DataAccessLayer.fluent
{

    public class Informed_entityFluent : IEntityTypeConfiguration<Informed_entity>
    {
        public void Configure(EntityTypeBuilder<Informed_entity> builder)
        {
            builder.ToTable("Informed_entity", "dbo");

            builder.HasKey(c => c.Id);

            builder.Property(c => c.CreationDate).IsRequired(true);
            builder.Property(c => c.UpdateTime).IsRequired(false);
            builder.Property(c => c.CessetionDate).IsRequired(false);

            builder.Property(c => c.Agency_Id).IsRequired(true).HasMaxLength(50);
            builder.Property(c => c.Rout_Id).IsRequired(true).HasMaxLength(50);
            builder.Property(c => c.Stop_Id).IsRequired(true).HasMaxLength(50);
            builder.Property(c => c.Route_Type).IsRequired(true);

            builder.HasOne<Alert>(g => g.Alert).WithMany(c => c.Informed_Entities).HasForeignKey(g => g.Alert_Id);




        }

    }
}

