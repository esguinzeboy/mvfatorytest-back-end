﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MVFactoryEvaluation.Enitys;
using System;
using System.Collections.Generic;
using System.Text;

namespace MVFactoryEvaluation.DataAccessLayer.fluent
{

    public class LineFluent : IEntityTypeConfiguration<Line>
    {
        public void Configure(EntityTypeBuilder<Line> builder)
        {
            builder.ToTable("Lines", "dbo");

            builder.HasKey(c => c.Id);

            builder.Property(c => c.CreationDate).IsRequired(true);
            builder.Property(c => c.UpdateTime).IsRequired(false);
            builder.Property(c => c.CessetionDate).IsRequired(false);

           // builder.Property(c => c.Trip_Id).IsRequired(true).HasMaxLength(10);
            builder.Property(c => c.Route_Id).IsRequired(true).HasMaxLength(10); 
            builder.Property(c => c.Direction_Id).IsRequired(true);
            builder.Property(c => c.Direction_Description).IsRequired(true).HasMaxLength(150); 

            builder.HasMany<Station>(g => g.Stations)
            .WithOne(s => s.Line)
           .HasForeignKey(s => s.Line_Id);

        }

    }
}

