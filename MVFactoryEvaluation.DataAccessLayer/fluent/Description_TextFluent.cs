﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MVFactoryEvaluation.Enitys;
using System;
using System.Collections.Generic;
using System.Text;

namespace MVFactoryEvaluation.DataAccessLayer.fluent
{

    public class Description_TextFluent : IEntityTypeConfiguration<Description_Text>
    {
        public void Configure(EntityTypeBuilder<Description_Text> builder)
        {
            builder.ToTable("Description_Text", "dbo");

            builder.HasKey(c => c.Id);

            builder.Property(c => c.CreationDate).IsRequired(true);
            builder.Property(c => c.UpdateTime).IsRequired(false);
            builder.Property(c => c.CessetionDate).IsRequired(false);

            // builder.Property(c => c.Trip_Id).IsRequired(true).HasMaxLength(10);
            builder.Property(c => c.Text).IsRequired(true).HasMaxLength(200);
            builder.Property(c => c.Language).IsRequired(true).HasMaxLength(200);
            //builder.Property(c => c.isHeader).IsRequired(true);

                        
            builder.HasOne<Alert>(g => g.Alert).WithMany(c => c.Description_Texts).HasForeignKey(g=> g.Alert_Id);



        }

    }
}

