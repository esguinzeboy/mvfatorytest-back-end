﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;

namespace MVFactoryEvaluation.DataAccessLayer
{
    public class ConnectionStringConfig
    {
        public string stringConnect;

        public ConnectionStringConfig()
        {
          //System.Diagnostics.Debugger.Launch();

            this.stringConnect = ConfigurationManager.AppSettings["connectionStrings.DBConnection"];
            if (this.stringConnect == null)
                this.stringConnect = "Data Source=JAVI-PC\\SQLEXPRESS;Initial Catalog=TestMvFactoryDB;Integrated Security=True";

        }

    }
}
