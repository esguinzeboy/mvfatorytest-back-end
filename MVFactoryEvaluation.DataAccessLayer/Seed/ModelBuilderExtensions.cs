﻿using Microsoft.EntityFrameworkCore;
using MVFactoryEvaluation.Enitys;
using System;
using System.Collections.Generic;
using System.Text;

namespace MVFactoryEvaluation.DataAccessLayer.Seed
{
    public static class ModelBuilderExtensions
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            // System.Diagnostics.Debugger.Launch();

            modelBuilder.Entity<Station>().HasData(
                new Station
                {
                    Id = 1,
                    Line_Id = 1,
                    Stop_id = "1059N",
                    Stop_name = "San Pedrito",
                    CreationDate = DateTime.Today,

                },

            new Station
            {
                Id = 2,
                Line_Id = 1,
                Stop_id = "1060N",
                Stop_name = "San José de Flores",
                CreationDate = DateTime.Today,
            },

            new Station
            {
                Id = 3,
                Line_Id = 1,
                Stop_id = "1061N",
                Stop_name = "Carabobo",
                CreationDate = DateTime.Today,
            },

            new Station
            {
                Id = 5,
                Line_Id = 1,
                Stop_id = "1062N",
                Stop_name = "Puan",
                CreationDate = DateTime.Today,
            },

            new Station
            {
                Id = 6,
                Line_Id = 1,
                Stop_id = "1063N",
                Stop_name = "PrimeraJunta",
                CreationDate = DateTime.Today,
            },

            new Station
            {
                Id = 7,
                Line_Id = 1,
                Stop_id = "1064N",
                Stop_name = "Acoyte",
                CreationDate = DateTime.Today,
            },

            new Station
            {
                Id = 8,
                Line_Id = 1,
                Stop_id = "1065N",
                Stop_name = "Rio de Janeiro",
                CreationDate = DateTime.Today,
            },

            new Station
            {
                Id = 9,
                Line_Id = 1,
                Stop_id = "1066N",
                Stop_name = "Castro Barro",
                CreationDate = DateTime.Today,
            });



            modelBuilder.Entity<Line>().HasData(
                new Line
                {
                    Id = 1,
                    Direction_Id = 1,
                    Route_Id = "LineaA",
                    Direction_Description = "San Pedrito - Plaza de Mayo",
                    CreationDate = DateTime.Today
                },

            new Line
            {
                Id = 2,
                Direction_Id = 0,
                Route_Id = "LineaA",
                Direction_Description = "Plaza de Mayo- San Pedrito",
                CreationDate = DateTime.Today
            },

            new Line
            {
                Id = 3,
                Direction_Id = 1,
                Route_Id = "LineaB",
                Direction_Description = "Rosas - Alem",
                CreationDate = DateTime.Today
            },

            new Line
            {
                Id = 4,
                Direction_Id = 0,
                Route_Id = "LineaB",
                Direction_Description = "Alem - Rosas",
                CreationDate = DateTime.Today
            },

            new Line
            {
                Id = 5,
                Direction_Id = 1,
                Route_Id = "LineaD",
                Direction_Description = "Congreso de Tucumán - Catedral",
                CreationDate = DateTime.Today
            },

            new Line
            {
                Id = 6,
                Direction_Id = 0,
                Route_Id = "LineaD",
                Direction_Description = "Bolivar -Virreyes",
                CreationDate = DateTime.Today
            },

            new Line
            {
                Id = 7,
                Direction_Id = 1,
                Route_Id = "LineaE",
                Direction_Description = "Virreyes - Bolivar",
                CreationDate = DateTime.Today
            },

            new Line
            {
                Id = 8,
                Direction_Id = 0,
                Route_Id = "LineaE",
                Direction_Description = "San Pedrito - Plaza de Mayo",
                CreationDate = DateTime.Today
            });


            modelBuilder.Entity<Cause>().HasData(
           new Cause
           {
               Id = 1,
               Cause_id = 1,
               Description = "Causa Desconocida",
               CreationDate = DateTime.Today
           },
            new Cause
            {
                Id = 2,
                Cause_id = 2,
                Description = "Otra Causa",
                CreationDate = DateTime.Today
            },
             new Cause
             {
                 Id = 3,
                 Cause_id = 3,
                 Description = "Problemas Tecnicos",
                 CreationDate = DateTime.Today
             },
              new Cause
              {
                  Id = 4,
                  Cause_id = 4,
                  Description = "Paro",
                  CreationDate = DateTime.Today
              },
               new Cause
               {
                   Id = 5,
                   Cause_id = 5,
                   Description = "Demostracion",
                   CreationDate = DateTime.Today
               },
                new Cause
                {
                    Id = 6,
                    Cause_id = 6,
                    Description = "Accidente",
                    CreationDate = DateTime.Today
                },
                 new Cause
                 {
                     Id = 7,
                     Cause_id = 7,
                     Description = "Vacaciones",
                     CreationDate = DateTime.Today
                 },
                  new Cause
                  {
                      Id = 8,
                      Cause_id = 8,
                      Description = "Clima",
                      CreationDate = DateTime.Today
                  },
                   new Cause
                   {
                       Id = 9,
                       Cause_id = 9,
                       Description = "Mantenimiento",
                       CreationDate = DateTime.Today
                   },
                    new Cause
                    {
                        Id = 10,
                        Cause_id = 10,
                        Description = "Obras",
                        CreationDate = DateTime.Today
                    },
                     new Cause
                     {
                         Id = 11,
                         Cause_id = 11,
                         Description = "Actividad Policial",
                         CreationDate = DateTime.Today
                     },
                       new Cause
                       {
                           Id = 12,
                           Cause_id = 12,
                           Description = "Emergencia Medica",
                           CreationDate = DateTime.Today
                       });



          


            modelBuilder.Entity<Effect>().HasData(
           new Effect
           {
               Id = 1,
               Effect_id = 1,
               Description = "Sin Servicio",
               CreationDate = DateTime.Today
           },
            new Effect
            {
                Id = 2,
                Effect_id = 2,
                Description = "Servicio Reducido",
                CreationDate = DateTime.Today
            },
             new Effect
             {
                 Id = 3,
                 Effect_id = 3,
                 Description = "Demoras",
                 CreationDate = DateTime.Today
             },
              new Effect
              {
                  Id = 4,
                  Effect_id = 4,
                  Description = "Desvio",
                  CreationDate = DateTime.Today
              },
               new Effect
               {
                   Id = 5,
                   Effect_id = 5,
                   Description = "Servicio Adicional",
                   CreationDate = DateTime.Today
               },
                new Effect
                {
                    Id = 6,
                    Effect_id = 6,
                    Description = "Servicio Modificado",
                    CreationDate = DateTime.Today
                },
                 new Effect
                 {
                     Id = 7,
                     Effect_id = 7,
                     Description = "Otros Effectos",
                     CreationDate = DateTime.Today
                 },
                  new Effect
                  {
                      Id = 8,
                      Effect_id = 8,
                      Description = "Effecto Desconocido",
                      CreationDate = DateTime.Today
                  },
                   new Effect
                   {
                       Id = 9,
                       Effect_id = 9,
                       Description = "Linea Parada",
                       CreationDate = DateTime.Today
                   });
             



        }
    }
}
